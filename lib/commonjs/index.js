"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactNative = require("react-native");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const PkCanvasView = (0, _reactNative.requireNativeComponent)('PkCanvasView'); // export default PkCanvasView;

class PKCanvasView extends _react.default.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "canvasViewRef", /*#__PURE__*/_react.default.createRef());

    _defineProperty(this, "generateImageAsBase64", () => {
      return new Promise(resolve => {
        const {
          current
        } = this.canvasViewRef;

        if (current) {
          const node = (0, _reactNative.findNodeHandle)(current);

          _reactNative.NativeModules.PkCanvasView.generateImageAsBase64(node, resolve);
        }
      });
    });

    _defineProperty(this, "save", () => {
      return new Promise(resolve => {
        const {
          current
        } = this.canvasViewRef;

        if (current) {
          const node = (0, _reactNative.findNodeHandle)(current);

          _reactNative.NativeModules.PkCanvasView.save(node, resolve);
        }
      });
    });

    _defineProperty(this, "focus", () => {
      const {
        current
      } = this.canvasViewRef;

      if (current) {
        const node = (0, _reactNative.findNodeHandle)(current);

        _reactNative.NativeModules.PkCanvasView.focus(node);
      }
    });
  }

  static getNotes() {
    return _reactNative.NativeModules.PkCanvasView.getNotes().then(raw => {
      // parse notes
      const notes = raw.notes.map(url => {
        const matches = url.match(/((\w{8})-(\d{3}))\.png$/);

        if ((matches === null || matches === void 0 ? void 0 : matches.length) === 4) {
          return {
            paper: matches[3],
            file: matches[1],
            url
          };
        } else {
          return null;
        }
      }).filter(exist => exist); // return

      return { ...raw,
        notes
      };
    });
  }

  render() {
    return /*#__PURE__*/_react.default.createElement(PkCanvasView, _extends({
      ref: this.canvasViewRef
    }, this.props));
  }

}

exports.default = PKCanvasView;
//# sourceMappingURL=index.js.map