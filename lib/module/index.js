function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from 'react';
import { requireNativeComponent, findNodeHandle, NativeModules } from 'react-native'; // types

const PkCanvasView = requireNativeComponent('PkCanvasView'); // export default PkCanvasView;

export default class PKCanvasView extends React.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "canvasViewRef", /*#__PURE__*/React.createRef());

    _defineProperty(this, "generateImageAsBase64", () => {
      return new Promise(resolve => {
        const {
          current
        } = this.canvasViewRef;

        if (current) {
          const node = findNodeHandle(current);
          NativeModules.PkCanvasView.generateImageAsBase64(node, resolve);
        }
      });
    });

    _defineProperty(this, "save", () => {
      return new Promise(resolve => {
        const {
          current
        } = this.canvasViewRef;

        if (current) {
          const node = findNodeHandle(current);
          NativeModules.PkCanvasView.save(node, resolve);
        }
      });
    });

    _defineProperty(this, "focus", () => {
      const {
        current
      } = this.canvasViewRef;

      if (current) {
        const node = findNodeHandle(current);
        NativeModules.PkCanvasView.focus(node);
      }
    });
  }

  static getNotes() {
    return NativeModules.PkCanvasView.getNotes().then(raw => {
      // parse notes
      const notes = raw.notes.map(url => {
        const matches = url.match(/((\w{8})-(\d{3}))\.png$/);

        if ((matches === null || matches === void 0 ? void 0 : matches.length) === 4) {
          return {
            paper: matches[3],
            file: matches[1],
            url
          };
        } else {
          return null;
        }
      }).filter(exist => exist); // return

      return { ...raw,
        notes
      };
    });
  }

  render() {
    return /*#__PURE__*/React.createElement(PkCanvasView, _extends({
      ref: this.canvasViewRef
    }, this.props));
  }

}
//# sourceMappingURL=index.js.map