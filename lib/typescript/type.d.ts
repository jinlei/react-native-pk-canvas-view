export interface CanvasNote {
    file: string;
    paper: string;
}
export interface GetNotesResponseRaw {
    error: number;
    notes: string[];
}
