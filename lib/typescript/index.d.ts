import React from 'react';
export default class PKCanvasView extends React.Component<any> {
    canvasViewRef: any;
    static getNotes(): any;
    generateImageAsBase64: () => Promise<unknown>;
    save: () => Promise<unknown>;
    focus: () => void;
    render(): JSX.Element;
}
