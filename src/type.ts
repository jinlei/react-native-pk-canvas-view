export interface CanvasNote {
  id: string;
  url: string;
}

export interface GetNotesResponseRaw {
  error: number;
  notes: string[];
}
