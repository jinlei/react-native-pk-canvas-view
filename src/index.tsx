import React from 'react';
import {
  requireNativeComponent,
  findNodeHandle,
  NativeModules,
} from 'react-native';
// types
import type { GetNotesResponseRaw, CanvasNote } from './type';

const PkCanvasView: any = requireNativeComponent('PkCanvasView');

// export default PkCanvasView;
export default class PKCanvasView extends React.Component<any> {
  canvasViewRef: any = React.createRef();

  static getNotes() {
    return NativeModules.PkCanvasView.getNotes().then(
      (raw: GetNotesResponseRaw) => {
        // parse notes
        const notes: CanvasNote[] = raw.notes
          .map((url: string): any => {
            const [match, id] = url.match(/([^/]*)\.png$/);
            return {
              id,
              url
            }
          })
        // return
        return {
          ...raw,
          notes,
        };
      }
    );
  }

  static removeNote (id: string) {
    return NativeModules.PkCanvasView.removeNote(id);
  }

  generateImageAsBase64 = () => {
    return new Promise((resolve) => {
      const { current } = this.canvasViewRef;
      if (current) {
        const node = findNodeHandle(current);
        NativeModules.PkCanvasView.generateImageAsBase64(node, resolve);
      }
    });
  };

  save = () => {
    return new Promise((resolve) => {
      const { current } = this.canvasViewRef;
      if (current) {
        const node = findNodeHandle(current);
        NativeModules.PkCanvasView.save(node, resolve);
      }
    });
  };

  focus = () => {
    const { current } = this.canvasViewRef;
    if (current) {
      const node = findNodeHandle(current);
      NativeModules.PkCanvasView.focus(node);
    }
  };

  render() {
    return <PkCanvasView ref={this.canvasViewRef} {...this.props} />;
  }
}
