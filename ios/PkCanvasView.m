#import <React/RCTBridgeModule.h>
#import <React/RCTViewManager.h>
#import <React/RCTEventEmitter.h>

@interface RCT_EXTERN_MODULE(PkCanvasView, RCTViewManager)
RCT_EXPORT_VIEW_PROPERTY(base64, NSString)
RCT_EXPORT_VIEW_PROPERTY(image, NSString)
RCT_EXPORT_VIEW_PROPERTY(onDrawingChanged, RCTBubblingEventBlock)
RCT_EXTERN_METHOD(save:(nonnull NSNumber *)reactTag
                  resolve:(RCTResponseSenderBlock)
)
RCT_EXTERN_METHOD(generateImageAsBase64:(RCTResponseSenderBlock) resolve)
RCT_EXTERN_METHOD(getNotes: (RCTPromiseResolveBlock)resolve reject:(RCTPromiseRejectBlock))
RCT_EXTERN_METHOD(focus:(nonnull NSNumber *)reactTag)
@end
