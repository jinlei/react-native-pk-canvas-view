//
//  PkCanvasView.swift
//  PkCanvasView
//
//  Created by Jin Lei on 31/12/20.
//  Copyright © 2020 Facebook. All rights reserved.
//

import Foundation
import UIKit;
import PencilKit

@available(iOS 13.0, *)
class CustomizedPKCanvasView: PKCanvasView {
    @objc var onDrawingChanged: RCTBubblingEventBlock!
    var image: String! = nil;
    var skipAutoSave: Bool! = true;
    
    @objc func setBase64 (_ base64: NSString) {
        
    }
    
    @objc func setImage (_ value: NSString) {
        image = value as String;
        do {
            self.drawing = try PKDrawing(data: Data(contentsOf: getFileURL(ext: "drawing")))
        } catch{}
    }
    
    func save() {
        let drawing = self.drawing;
        if (!drawing.bounds.isEmpty && !self.bounds.isEmpty && image != nil) {
            do {
                try drawing.dataRepresentation().write(to: getFileURL(ext: "drawing"))
                try drawing.image(from: self.bounds, scale: UIScreen.main.scale).pngData()?.write(to: getFileURL(ext: "png"))
            } catch {}
        }
    }
    
    // MASK: utils
    func getFileURL (ext: String) -> URL {
        let folder = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("notes", isDirectory: true);
        do {
            try FileManager.default.createDirectory(at: folder, withIntermediateDirectories: true, attributes: nil);
        } catch {}
        let file = URL(fileURLWithPath: image, relativeTo: folder).appendingPathExtension(ext);
        return file;
    }
}

@available(iOS 13.0, *)
@objc(PkCanvasView)
class PkCanvasView:RCTViewManager,PKCanvasViewDelegate,PKToolPickerObserver {
    var toolPicker: PKToolPicker!
    var canvasView: CustomizedPKCanvasView!
    var drawingBounds: CGRect!
    @objc var image:NSString!
    
    @objc override static func requiresMainQueueSetup() -> Bool {
        return true;
    }
    
    override func view() -> UIView {
        if #available(iOS 14.0, *) {
            canvasView = CustomizedPKCanvasView()
            canvasView.delegate = self;
            canvasView.bounces = false;
            canvasView.allowsFingerDrawing = false;
            // toolPicker init
            toolPicker = PKToolPicker();
            toolPicker.addObserver(canvasView);
            toolPicker.addObserver(self);
            toolPicker.setVisible(true, forFirstResponder: canvasView);
            toolPicker.showsDrawingPolicyControls = false;
            canvasView.becomeFirstResponder();
            return canvasView;
        } else {
            // Fallback on earlier versions
            return UIView();
        };
    }
    // MARK: Props
    // MARK: Canvas View Delegate Events
    /// Delegate method: Note that the drawing has changed.
    internal func canvasViewDrawingDidChange(_ canvasView: PKCanvasView) {
        let drawing = canvasView.drawing;
        // save current drawing content
        self.canvasView.save();
        // update canvas size
        let contentWidth = max(canvasView.bounds.width, drawing.bounds.width);
        var contentHeight = max(canvasView.frame.height, 200);
        if (!drawing.bounds.isNull) {
            contentHeight = drawing.bounds.maxY + contentHeight;
        }
        // trigger content changed
        if (self.canvasView.onDrawingChanged != nil) {
            self.canvasView.onDrawingChanged([
                "contentWidth": contentWidth,
                "contentHeight": contentHeight
            ]);
        }
    }

    
    // MARK: Plugin Methods
    @objc
    func generateImageAsBase64 (_ reactTag: NSNumber, resolve: @escaping RCTResponseSenderBlock) -> Void {
        self.bridge.uiManager.addUIBlock { uiManager, viewRegistry in
            guard let canvasView = viewRegistry?[reactTag] as? CustomizedPKCanvasView else {
                if (RCT_DEV == 1) {
                    print("Invalid view returned from registry, expecting ContainerView")
                }
                return
            }
            let image = canvasView.drawing.image(from: canvasView.bounds, scale: 1);
            resolve([[
                "error": 0,
                "base64": image.pngData()?.base64EncodedString()
            ]]);
        }
    }
    
    @objc
    func getNotes(_ resolve: @escaping RCTPromiseResolveBlock, reject: RCTPromiseRejectBlock) -> Void {
        let fileManager = FileManager.default;
        let folder = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("notes", isDirectory: true);
        do {
            try FileManager.default.createDirectory(at: folder, withIntermediateDirectories: true, attributes: nil);
        } catch {}
        do {
            let fileURLs = try fileManager.contentsOfDirectory(at: folder, includingPropertiesForKeys: nil).filter {$0.pathExtension == "png"}
            resolve([
                "error": 0,
                "notes": fileURLs.map{$0.absoluteString}
            ])
            
        } catch {
            print("Error while enumerating files \(folder.path): \(error.localizedDescription)")
        }
    }
    
    @objc
    func focus(_ reactTag: NSNumber) {
        self.bridge.uiManager.addUIBlock { uiManager, viewRegistry in
            guard let canvasView = viewRegistry?[reactTag] as? CustomizedPKCanvasView else {
                if (RCT_DEV == 1) {
                    print("Invalid view returned from registry, expecting ContainerView")
                }
                return
            }
            canvasView.becomeFirstResponder();
        }
    }
}
