# react-native-pk-canvas-view

A PKCanvasView of React Native

## Installation

```sh
npm install react-native-pk-canvas-view
```

## Usage

```js
import PkCanvasView from "react-native-pk-canvas-view";

// ...

const result = await PkCanvasView.multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
