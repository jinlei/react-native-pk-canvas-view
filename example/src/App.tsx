import React from 'react';

import {
  StyleSheet,
  SafeAreaView,
  View,
  Button,
  Text,
  Image,
  Alert,
  ScrollView,
} from 'react-native';
import PKCanvasView from 'react-native-pk-canvas-view';

export default class App extends React.Component {
  canvasViewRef = React.createRef();
  state = {
    message: '',
    notes: [],
  };

  handleDrawingChanged = (e) => {
    this.setState({
      message: JSON.stringify(e.nativeEvent),
    });
  };

  handleBase64Press = () => {
    const { current } = this.canvasViewRef;
    if (current) {
      current.generateImageAsBase64().then((message) => {
        this.setState({
          message: JSON.stringify(message),
        });
      });
    }
  };

  render() {
    const { message, base64, notes } = this.state;

    return (
      <SafeAreaView style={styles.container}>
        <ScrollView style={styles.body} bounces={false}>
          <PKCanvasView
            ref={this.canvasViewRef}
            color="#32a852"
            image="image1"
            style={styles.canvasView}
            onDrawingChanged={this.handleDrawingChanged}
          />
        </ScrollView>
        <View style={styles.footer}>
          <Text>{message}</Text>
          <Button title="Generate Base64" onPress={this.handleBase64Press} />
        </View>
      </SafeAreaView>
    );
  }

  componentDidMount() {
    // PKCanvasView.getNotes().then(({ notes }) => {
    //   console.tron.log('notes', notes);
    //   this.setState({
    //     notes,
    //   });
    // });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body: {
    flex: 1,
    backgroundColor: 'pink',
  },
  canvasView: {
    minHeight: 500,
  },
  footer: {},
});
